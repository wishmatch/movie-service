package com.wishmatch.movieservice.repository;

import com.wishmatch.movieservice.dao.entity.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<User, String> {

    User findBySso(String sso);

}
