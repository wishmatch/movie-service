package com.wishmatch.movieservice.repository;

import com.wishmatch.movieservice.dao.entity.Counter;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class CounterRepository {

    private final MongoTemplate mongoTemplate;

    public long increaseCounter(String counterName) {

        var query = new Query(Criteria.where("name").is(counterName));
        var update = new Update().inc("sequence", 1);
        var counter = mongoTemplate.findAndModify(query, update, Counter.class);

        return counter.getSequence();
    }

}
