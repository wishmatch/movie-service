package com.wishmatch.movieservice.repository;

import com.wishmatch.movieservice.dao.entity.Movie;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieRepository extends MongoRepository<Movie, String> {

    Movie findByName(String name);

}
