package com.wishmatch.movieservice.scheduled;

import com.wishmatch.movieservice.dao.external.KpMovieShortResponse;
import com.wishmatch.movieservice.mapper.MovieMapper;
import com.wishmatch.movieservice.repository.MovieRepository;
import com.wishmatch.movieservice.service.external.KpApiService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class MovieScheduled {

    private final KpApiService kpApiService;
    private final MovieRepository movieRepository;

    private final MovieMapper movieMapper;

    /**
     * В начале каждого часа делает запрос на получение фильмов
     */
    @Scheduled(cron = "0 0 * * * ?")
    public void scheduleGetMovies() {
        var response = kpApiService.getMovies();
        log.info("Выполнен запрос на получение фильмов в KinopoiskAPI");

        var movies = Optional.ofNullable(response)
                .map(KpMovieShortResponse::getDocs)
                .stream()
                .flatMap(Collection::stream)
                .map(movieMapper::map)
                .toList();
        movieRepository.saveAll(movies);
        log.info("{} фильмов сохранено", movies.size());
    }

}
