package com.wishmatch.movieservice.service.external;

import com.wishmatch.movieservice.dao.external.KpMovieShortResponse;
import com.wishmatch.movieservice.repository.CounterRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClient;

@Slf4j
@Service
@RequiredArgsConstructor
public class KpApiService {

    private static final String GET_MOVIES = "/v1.4/movie";

    private final RestClient restClient;

    private final CounterRepository counterRepository;

    @Value("${kinopoisk-api.header.name}")
    private String headerName;
    @Value("${kinopoisk-api.header.value}")
    private String headerValue;
    @Value("${kinopoisk-api.limit}")
    private String limit;

    public KpMovieShortResponse getMovies() {
        long page = counterRepository.increaseCounter("page");

//        log.info("Запрос в сервис KinoposkApi, токен {}", headerValue);
        KpMovieShortResponse response = restClient.get()
                .uri(uriBuilder -> uriBuilder
                        .scheme("https")
                        .host("api.kinopoisk.dev")
                        .path(GET_MOVIES)
                        .queryParam("page", String.valueOf(page))
                        .queryParam("limit", limit)
                        .build())
                .header(headerName, headerValue)
                .retrieve()
                .body(KpMovieShortResponse.class);
        log.info("Запрос выполнен: total - {}, limit - {}, page - {}, pages - {}",
                response.getTotal(), response.getLimit(), response.getPage(), response.getPages());

        return response;
    }

}
