package com.wishmatch.movieservice.service;

import com.wishmatch.movieservice.dao.dto.MovieShortDto;
import com.wishmatch.movieservice.dao.entity.Movie;
import com.wishmatch.movieservice.dao.external.KpMovieShortResponse;
import com.wishmatch.movieservice.mapper.MovieMapper;
import com.wishmatch.movieservice.repository.MovieRepository;
import com.wishmatch.movieservice.repository.UserRepository;
import com.wishmatch.movieservice.service.external.KpApiService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MovieService {

    private final MongoOperations mongoOperations;

    private final KpApiService kpApiService;

    private final MovieRepository movieRepository;
    private final UserRepository userRepository;

    private final MovieMapper movieMapper;

    public List<MovieShortDto> getMovies() {
        var response = kpApiService.getMovies();
        var movies = Optional.ofNullable(response)
                .map(KpMovieShortResponse::getDocs)
                .stream()
                .flatMap(Collection::stream)
                .map(movieMapper::map)
                .toList();
        movieRepository.saveAll(movies);

        return movieMapper.toDto(movies);
    }

    /**
     * Способы оптимизации в будущем
     * <p>
     * 1. **Предварительная фильтрация**: Вместо того чтобы выбирать случайный фильм и затем проверять, просмотрен ли он пользователем, вы можете заранее удалить из выборки просмотренные фильмы. Например, вы можете периодически обновлять список непросмотренных фильмов для каждого пользователя и затем выбирать случайный фильм из этого списка.
     * <p>
     * 2. **Хранение просмотренных фильмов на клиенте**: Вместо хранения списка просмотренных фильмов на сервере, вы можете хранить его на клиенте. Тогда клиент сможет передавать этот список назад серверу при запросе следующего случайного фильма.
     * <p>
     * 3. **Разделение на подмножества**: Можно разделить список фильмов на подмножества и назначить каждому пользователю конкретное подмножество для просмотра. Потом, когда пользователь проходит через все фильмы в его подмножестве, можно назначить ему новое. Это не только сократит количество фильмов, которые нужно обрабатывать при каждом запросе, но также снизит шансы того, что два пользователя получат один и тот же фильм.
     * <p>
     * 4. **Балансировка нагрузки**: Если текущий подход создает слишком большую нагрузку на базу данных MongoDB, вы можете рассмотреть возможность использования дополнительной базы данных или кэша для хранения списка просмотренных фильмов, например, Redis.
     * <p>
     * 5. ** Использование сложной логики отбора **: Вместо случайного выбора можно иметь более сложную логику отбора, которая включает в себя нейронные сети и машинное обучение для рекомендации фильмов.
     * <p>
     * 6. **Использование индексов в MongoDB**: Делайте уверены, что вы используете индексы в MongoDB для улучшения производительности запросов. Если вы выполняете поиск фильмов по ID, убедитесь, что вы создали соответствующий индекс.
     * <p>
     * 7. **Пагинация**: Если у вас есть большое количество фильмов, вы можете рассмотреть использование пагинации для ограничения количества фильмов, которые вы получаете с каждым запросом. Потом примените случайный выбор на этом ограниченном списке.
     */
    public MovieShortDto getRandomMovie() {
        //sso будет приходить по grpc от UserService
        var user = userRepository.findBySso("volobuevrs");
        List<Long> viewedMoviesIds = user.getViewed();

        Aggregation agg = Aggregation.newAggregation(
                Aggregation.match(Criteria.where("kpId").nin(viewedMoviesIds)), Aggregation.sample(1)
        );
        Movie movie = mongoOperations.aggregate(agg, "movies", Movie.class).getUniqueMappedResult();

        if (movie != null) {
            user.getViewed().add(movie.getKpId());
            userRepository.save(user);
        }

        return movieMapper.toDto(movie);
    }

}
