package com.wishmatch.movieservice.dao.dto;

public record MovieShortDto(
        Long kpId,
        String name,
        String type,
        Integer year,
        String shortDescription,
        Integer movieLength,
        Float rating,
        String poster
) {
}
