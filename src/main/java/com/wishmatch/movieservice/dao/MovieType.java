package com.wishmatch.movieservice.dao;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

@Getter
@AllArgsConstructor
public enum MovieType {

    movie("movie", "фильм"),
    tv_series("tv-series", "сериал"),
    cartoon("cartoon", "мультфильм"),
    anime("anime", "аниме"),
    animated_series("animated-series", "мультсериал"),
    tv_show("tv_show", "телешоу");

    private String eng;
    private String rus;

    private static Map<String, MovieType> engMap = Arrays.stream(MovieType.values())
            .collect(Collectors.toMap(MovieType::getEng, e -> e));

    public static String getRusByEng(String eng) {
        return engMap.get(eng).rus;
    }

}
