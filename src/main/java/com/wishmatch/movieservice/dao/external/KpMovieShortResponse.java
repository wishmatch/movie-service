package com.wishmatch.movieservice.dao.external;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class KpMovieShortResponse {

    @JsonProperty("docs")
    private List<Doc> docs;
    @JsonProperty("total")
    private int total;
    @JsonProperty("limit")
    private int limit;
    @JsonProperty("page")
    private int page;
    @JsonProperty("pages")
    private int pages;

    @Data
    public static class Doc {
        @JsonProperty("id")
        private Long id;
        @JsonProperty("name")
        private String name;
        @JsonProperty("type")
        private String type;
        @JsonProperty("year")
        private Integer year;
        @JsonProperty("shortDescription")
        private String shortDescription;
        @JsonProperty("movieLength")
        private Integer movieLength;
        @JsonProperty("rating")
        private Rating rating;
        @JsonProperty("poster")
        private ShortImage poster;

        @Data
        public static class Rating {
            @JsonProperty("kp")
            private Float kp;
        }

        @Data
        public static class ShortImage {
            @JsonProperty("url")
            private String url;
        }

    }

}
