package com.wishmatch.movieservice.dao.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document("movies")
public class Movie {

    @Id
    private String id;
    @Indexed
    private Long kpId;
    @Indexed
    private String name;
    private String type;
    private Integer year;
    private String shortDescription;
    private Integer movieLength;
    private Rating rating;
    private String poster;

    @Data
    public static class Rating {
        private Float kp;
    }

}
