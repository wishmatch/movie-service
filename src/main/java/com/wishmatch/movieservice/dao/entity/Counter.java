package com.wishmatch.movieservice.dao.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document("counters")
public class Counter {
    @Id
    private String id;
    private String name;
    private long sequence;
}
