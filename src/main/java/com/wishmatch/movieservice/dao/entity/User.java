package com.wishmatch.movieservice.dao.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Document("users")
public class User {

    @Id
    private String id;
    @Indexed
    private String sso;
    private List<Long> viewed;

}
