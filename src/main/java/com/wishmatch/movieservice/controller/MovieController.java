package com.wishmatch.movieservice.controller;

import com.wishmatch.movieservice.dao.dto.MovieShortDto;
import com.wishmatch.movieservice.service.MovieService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/movie", produces = MediaType.APPLICATION_JSON_VALUE)
public class MovieController {

    private final MovieService movieService;

    @GetMapping
    public List<MovieShortDto> getMovies() {
        return movieService.getMovies();
    }

    @GetMapping("/random")
    public MovieShortDto getRandomMovie() {
        return movieService.getRandomMovie();
    }

}
