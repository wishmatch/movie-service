package com.wishmatch.movieservice.mapper;

import com.wishmatch.movieservice.dao.MovieType;
import com.wishmatch.movieservice.dao.dto.MovieShortDto;
import com.wishmatch.movieservice.dao.entity.Movie;
import com.wishmatch.movieservice.dao.external.KpMovieShortResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring",
        imports = MovieType.class)
public interface MovieMapper {

    @Mapping(target = "rating", source = "rating.kp")
    @Mapping(target = "type", expression = "java( MovieType.getRusByEng(source.getType()) )")
    MovieShortDto toDto(Movie source);

    List<MovieShortDto> toDto(List<Movie> source);

    @Mapping(target = "kpId", source = "id")
    @Mapping(target = "poster", source = "poster.url")
    Movie map(KpMovieShortResponse.Doc source);

}
