//package com.wishmatch.movieservice.controller;
//
//import com.wishmatch.movieservice.BaseIntegrationTest;
//import com.wishmatch.movieservice.dao.entity.Movie;
//import com.wishmatch.movieservice.repository.MovieRepository;
//import org.junit.jupiter.api.AfterEach;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.junit.jupiter.MockitoExtension;
//import org.springframework.beans.factory.annotation.Autowired;
//
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//@ExtendWith(MockitoExtension.class)
//public class MovieControllerTest extends BaseIntegrationTest {
//
//    @Autowired
//    private MovieRepository movieRepository;
//
//    @BeforeEach
//    void setUp() {
//        Movie movie = new Movie();
//        movie.setId(1L);
//        movie.setName("Brothers");
//        movieRepository.save(movie);
//    }
//
//    @AfterEach
//    void tearDown() {
//        movieRepository.deleteAll();
//    }
//
//    @Test
//    void getMovies() throws Exception {
//        mockMvc.perform(get("/movie/Brothers"))
//                .andExpect(status().isOk())
//                .andExpect(content().string("{\"id\":1,\"name\":\"Brothers\"}"));
//    }
//
//}
