# Build stage
FROM gradle:jdk21-jammy AS build

# Установить рабочую директорию
WORKDIR /home/gradle/src

# Скопировать все необходимые файлы
COPY --chown=gradle:gradle . .

# Выполнить сборку с отключенными тестами
RUN gradle clean build -x test --no-daemon

# Package stage
FROM openjdk:21-jdk-slim

# Установить рабочую директорию
WORKDIR /app

# Скопировать скомпилированный JAR файл из build stage
COPY --from=build /home/gradle/src/build/libs/*.jar ./app.jar

# Запустить приложение
CMD ["java", "-jar", "/app/app.jar"]